
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Or


class Inpatient(metaclass=PoolMeta):
    __name__ = "health.inpatient"

    STATES = {
        'readonly': Or(Eval('state') == 'done', Eval('state') == 'finished')
    }
    roundings = fields.One2Many('health.patient.rounding', 'registration',
        'Roundings', states=STATES)
    nursing_notes = fields.One2Many('health.nursing.note', 'registration',
        'Nursing Notes')
    medication_log = fields.Function(fields.One2Many(
        'health.inpatient.medication.log', 'medication', "Log History"),
        'get_medication_log'
    )
    nutrition_control = fields.One2Many('health.patient.nutrition_control',
        'registration', 'Nutrition Control', states=STATES)
    water_balance = fields.One2Many('health.patient.water_balance',
        'registration', 'Water Balance', states=STATES)

    def get_medication_log(self, name=None):
        res = []
        for med in self.medications:
            res = [med_log.id for med_log in med.log_history]
        return res


class PatientNutritionControl(ModelSQL, ModelView):
    'Patient Nutrition Control'
    __name__ = 'health.patient.nutrition_control'

    registration = fields.Many2One('health.inpatient', 'Inpatient Reg.',
        required=True)
    date_time = fields.DateTime('Date Time', required=True)
    quantity = fields.Char('Quantity')
    application_route = fields.Char('Application Route')
    tolerance = fields.Selection([
        ('yes', 'Yes'),
        ('no', 'No'),
    ], 'Tolerance')
    notes = fields.Text('Notes')


class PatientWaterBalance(ModelSQL, ModelView):
    'Patient Water Balance'
    __name__ = 'health.patient.water_balance'

    registration = fields.Many2One('health.inpatient', 'Inpatient Reg.',
        required=True)
    date = fields.Date('Date', required=True)
    total_outflow = fields.Function(fields.Float('Total Outflow'),
        'get_outflow')
    total_inflow = fields.Function(fields.Float('Total Inflow'), 'get_inflow')
    total_balance = fields.Function(fields.Float('Total Balance'), 'get_balance')
    water_balance_shifts = fields.One2Many('health.patient.water_balance.shift',
        'water_balance', 'Water Balance Shift')

    def get_outflow(self, name=None):
        res = 0
        return res

    def get_inflow(self, name=None):
        res = 0
        return res

    def get_balance(self, name=None):
        if self.total_outflow and self.total_inflow:
            return self.total_outflow - self.total_inflow


class PatientWaterBalanceShift(ModelSQL, ModelView):
    'Patient Water Balance Shift'
    __name__ = 'health.patient.water_balance.shift'
    water_balance = fields.Many2One('health.patient.water_balance',
        'Water Balance', required=True)
    health_professional = fields.Many2One('health.professional', 'Nurse')
    stime = fields.Time('Time', required=True)
    solution = fields.Integer('Solution')
    meds = fields.Integer('Meds')
    vo = fields.Integer('Vo')
    in_other1 = fields.Integer('In Other')
    in_other2 = fields.Integer('In Other')
    shift = fields.Selection([
        (None, ''),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ], 'Shift')
    diuresis = fields.Integer('Diuresis')
    evac = fields.Integer('Evac.')
    out_other1 = fields.Integer('Out Other')
    out_other2 = fields.Integer('Out Other')
    # outflow = fields.Function(fields.Float('Total Outflow'),
    #     'get_outflow')
    # inflow = fields.Function(fields.Float('Total Inflow'), 'get_inflow')
