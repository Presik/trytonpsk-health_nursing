# -*- coding: utf-8 -*-
import pytz
from trytond.model import ModelView, ModelSQL, fields
from datetime import datetime
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, Equal
from trytond.i18n import gettext
from trytond.exceptions import UserWarning, UserError

# Class : PatientRounding
# Assess the patient and evironment periodically
# Usually done by nurses


class PatientRounding(ModelSQL, ModelView):
    'Patient Rounding'
    __name__ = 'health.patient.rounding'

    STATES = {'readonly': Eval('state') == 'done'}
    registration = fields.Many2One('health.inpatient',
        'Inpatient Reg.', required=True, states=STATES)
    code = fields.Char('Code',  states=STATES)
    health_professional = fields.Many2One('health.professional',
        'Health Professional', readonly=True)
    evaluation_start = fields.DateTime('Start', required=True, states=STATES)
    evaluation_end = fields.DateTime('End', readonly=True)
    state = fields.Selection([
        (None, ''),
        ('draft', 'In Progress'),
        ('done', 'Done'),
        ], 'State', readonly=True)
    shift = fields.Selection([
        (None, ''),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ], 'Shift',  states=STATES)
    environmental_assessment = fields.Char('Environment', states=STATES,
         help="Environment assessment. State any disorder in the room.")
    weight = fields.Integer('Weight', states=STATES,
        help="Measured weight in kg")
    general_condition = fields.Selection([
        (None, ''),
        ('stable', 'Stable'),
        ('critical', 'Critical'),
        ('delicate', 'Delicate'),
        ('serious', 'Serious'),
        ], 'General Condition',  states=STATES)
    consciousness_state = fields.Selection([
        (None, ''),
        ('oriented', 'Oriented'),
        ('alert', 'Alert'),
        ('oriented_alert', 'Oriented / Alert'),
        ('somnolent', 'Somnolent'),
        ('stuporous', 'Stuporous'),
        ('coma', 'Coma'),
        ], 'Consciousness State',  states=STATES)

    # hypotension = fields.Boolean('Hypotension')
    oxigen = fields.Selection([
        (None, ''),
        ('no', 'No'),
        ('nasal_cannula', 'Nasal Cannula'),
        ('mask', 'Mask'),
        ('tube', 'Tube'),
        ], 'Oxygen',  states=STATES)
    pregnant = fields.Selection([
        (None, ''),
        ('no', 'No'),
        ('preterm', 'Preterm'),
        ('in_term', 'In Term'),
        ('lp', 'LP'),
        ], 'Pregnant',  states=STATES)
    curettage = fields.Boolean('Curettage',  states=STATES)
    transvaginal_fluid = fields.Selection([
        ('', ''),
        ('normal', 'Normal'),
        ('slight', 'Slight'),
        ('abundant', 'Abundant'),
        ], 'Transvaginal Fluid',  states=STATES)
    bleeding = fields.Selection([
        ('', ''),
        ('no', 'No'),
        ('slight', 'Slight'),
        ('abundant', 'Abundant'),
        ('transvaginal', 'Transvaginal'),
        ('sanguinolento', 'Sanguinolento'),
        ], 'Bleeding',  states=STATES)
    pain_intention = fields.Selection([
        ('', ''),
        ('obstetric', 'Obstetric'),
        ('no_pain', 'No Pain'),
        ('slight_pain', 'Slight Pain'),
        ('intense_pain', 'Intense Pain'),
        ], 'Pain Intention',  states=STATES)
    heart_monitor = fields.Boolean('Heart Monitor',  states=STATES)
    infusion_pump = fields.Boolean('Infusion Pump',  states=STATES)
    fetal_monitor = fields.Boolean('Fetal Monitor',  states=STATES)
    feeding = fields.Selection([
        ('', ''),
        ('nxb', 'NXB'),
        ('without_help', 'Without Help'),
        ('with_help', 'With Help'),
        ('nasagastric_path', 'Nasagastric Path'),
        ('gastronomy', 'Gastronomy'),
        ('parental_nutrition', 'Parental Nutrition'),
        ], 'Feeding',  states=STATES)
    mobilization = fields.Selection([
        ('', ''),
        ('ambulatory', 'Ambulatory'),
        ('assisted', 'Assisted'),
        ('from_bed_chair', 'From Bed to Chair'),
        ('in_bed', 'In Bed'),
        ], 'Mobilization',  states=STATES)
    channeling = fields.Selection([
        ('', ''),
        ('yes', 'Yes'),
        ('no', 'No'),
        ('peripheral_vein', 'Peripheral Vein'),
        ('central_vein', 'Central Vein'),
        ('rechanneling', 'Rechanneling'),
        ], 'Channeling',  states=STATES)
    transfusion = fields.Selection([
        ('', ''),
        ('globular_packs', 'Globular Packs'),
        ('no', 'No'),
        ('whole_blood', 'Whole Blood'),
        ('plasma', 'Plasma'),
        ], 'Transfusion',  states=STATES)
    care = fields.Selection([
        ('', ''),
        ('enf', 'ENF'),
        ('central_path', 'Central Path'),
        ('surgical_wound', 'Surgical Wound'),
        ('breast', 'Breast'),
        ('genitals', 'Genitals'),
        ('catheters', 'Catheters'),
        ], 'Care',  states=STATES)
    drains = fields.Selection([
        ('', ''),
        ('no', 'No'),
        ('hemo_vac', 'Hemo-Vac'),
        ('pleuro-vac', 'Pleuro-Vac'),
        ('epidural', 'Epidural'),
        ], 'Drains',  states=STATES)
    diuresis = fields.Selection([
        ('', ''),
        ('fowley_probe', 'Fowley Probe,'),
        ('supra_probe', 'Supra Probe'),
        ], 'Diuresis',  states=STATES)
    gastric = fields.Selection([
        ('', ''),
        ('fowley_probe', 'NGLS,'),
        ('vomiting_nausea', 'Vomiting / Nausea'),
        ('Nausea', 'Nausea'),
        ], 'Gastric',  states=STATES)
    communication = fields.Selection([
        ('', ''),
        ('not_very', 'Not Very'),
        ('communicative', 'Communicative'),
        ], 'Communication',  states=STATES)
    sampling = fields.Boolean('Sampling',  states=STATES)
    imaging_test_pending = fields.Boolean('Imaging Test Pending',  states=STATES)
    wound_healing = fields.Selection([
        ('', ''),
        ('yes', 'Yes'),
        ('no', 'No'),
        ('pending', 'Pending'),
        ], 'Wound Healing',  states=STATES)
    born_situation = fields.Selection([
        (None, ''),
        ('no', 'No'),
        ('nicu', 'NICU'),
        ('neonate', 'Neonate'),
        ('altogether', 'Altogether, '),
        ], 'Born Situation',  states=STATES)
    bathroom = fields.Selection([
        (None, ''),
        ('unassisted_shower', 'Unassisted Shower'),
        ('assisted_shower', 'Assisted Shower'),
        ('bed', 'Bed'),
        ], 'Bathroom',  states=STATES)
    loc = fields.Integer('Glasgow', states=STATES,
        help='Level of Consciousness - on Glasgow Coma Scale :  < 9 severe -'
        ' 9-12 Moderate, > 13 minor',)
    loc_eyes = fields.Selection([
        (None, ''),
        ('1', 'Does not Open Eyes'),
        ('2', 'Opens eyes in response to painful stimuli'),
        ('3', 'Opens eyes in response to voice'),
        ('4', 'Opens eyes spontaneously'),
        ], 'Glasgow - Eyes', sort=False,
        states=STATES)
    loc_verbal = fields.Selection([
        (None, ''),
        ('1', 'Makes no sounds'),
        ('2', 'Incomprehensible sounds'),
        ('3', 'Utters inappropriate words'),
        ('4', 'Confused, disoriented'),
        ('5', 'Oriented, converses normally'),
        ], 'Glasgow - Verbal', sort=False,
        states=STATES)
    loc_motor = fields.Selection([
        (None, ''),
        ('1', 'Makes no movement'),
        ('2', 'Extension to painful stimuli - decerebrate response -'),
        ('3', 'Abnormal flexion to painful stimuli (decorticate response)'),
        ('4', 'Flexion / Withdrawal to painful stimuli'),
        ('5', 'Localizes painful stimuli'),
        ('6', 'Obeys commands'),
        ], 'Glasgow - Motor', sort=False, states=STATES)
    # The 6 P's of rounding
    pain = fields.Boolean('Pain',
        help="Check if the patient is in pain", states=STATES)
    pain_level = fields.Integer('Pain', help="Enter the pain level, from 1 to "
            "10", states={'invisible': ~Eval('pain'),
            'readonly': Eval('state') == 'done'})
    potty = fields.Boolean('Potty', help="Check if the patient needs to "
        "urinate / defecate", states=STATES)
    position = fields.Boolean('Position', help="Check if the patient needs to "
        "be repositioned or is unconfortable", states=STATES)
    proximity = fields.Boolean('Proximity', help="Check if personal items, "
        "water, alarm, ... are not in easy reach",states=STATES)
    pump = fields.Boolean('Pumps', help="Check if there is any issues with "
        "the pumps - IVs ... ", states=STATES)
    personal_needs = fields.Boolean('Personal needs', help="Check if the "
        "patient requests anything", states=STATES)

    # Vital Signs
    systolic = fields.Integer('Systolic Pressure', states=STATES)
    diastolic = fields.Integer('Diastolic Pressure', states=STATES)
    bpm = fields.Integer('Heart Rate',
        help='Heart rate expressed in beats per minute', states=STATES)
    respiratory_rate = fields.Integer('Respiratory Rate',
        help='Respiratory rate expressed in breaths per minute', states=STATES)
    osat = fields.Integer('Oxygen Saturation',
        help='Oxygen Saturation(arterial).', states=STATES)
    temperature = fields.Float('Temperature',
        help='Temperature in celsius', states=STATES)

    # Diuresis
    diuresis = fields.Integer('Diuresis', help="volume in ml", states=STATES)
    urinary_catheter = fields.Boolean('Urinary Catheter', states=STATES)
    glycemia = fields.Integer('Glycemia', help='Blood Glucose level',
        states=STATES)
    depression = fields.Boolean('Depression signs', help="Check this if the "
        "patient shows signs of depression", states=STATES)

    '''evolution = fields.Selection([
        (None, ''),
        ('n', 'Status Quo'),
        ('i', 'Improving'),
        ('w', 'Worsening'),
        ], 'Evolution', help="Check your judgement of current "
        "patient condition", sort=False, states=STATES)'''

    round_summary = fields.Text('Round Summary', states=STATES)
    signed_by = fields.Many2One(
        'health.professional', 'Signed by', readonly=True,
        states={'invisible': Equal(Eval('state'), 'draft')},
        help="Health Professional that signed the rounding")
    warning = fields.Boolean('Warning', help="Check this box to alert the "
        "supervisor about this patient rounding. A warning icon will be shown "
        "in the rounding list", states=STATES)
    warning_icon = fields.Function(fields.Char('Warning Icon'), 'get_warn_icon')
    procedures = fields.One2Many('health.rounding_procedure', 'rounding',
        'Procedures', help="List of the procedures in this rounding. Please "
        "enter the first one as the main procedure", states=STATES)
    report_start_date = fields.Function(fields.Date('Start Date'),
        'get_report_start_date')
    report_start_time = fields.Function(fields.Time('Start Time'),
        'get_report_start_time')
    report_end_date = fields.Function(fields.Date('End Date'),
        'get_report_end_date')
    report_end_time = fields.Function(fields.Time('End Time'),
        'get_report_end_time')
    medications_plan = fields.Function(fields.One2Many(
        'health.inpatient.medication', 'registration', 'Medications Plan'),
        'get_medications')
    medications = fields.One2Many('health.inpatient.medication.log',
        'rounding', 'Medications Log', context={
            'registration': Eval('registration'),
        })

    @staticmethod
    def default_health_professional():
        HealthProf = Pool().get('health.professional')
        return HealthProf.get_health_professional()

    @staticmethod
    def default_evaluation_start():
        return datetime.now()

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def __setup__(cls):
        super(PatientRounding, cls).__setup__()
        cls._buttons.update({
            'end_rounding': {
                'invisible': ~Eval('state').in_(['draft']),
            }})

        cls._order.insert(0, ('evaluation_start', 'DESC'))

    @classmethod
    @ModelView.button
    def end_rounding(cls, roundings):
        sign_prof = Pool().get('health.professional').get_health_professional()
        cls.write(roundings, {
            'state': 'done',
            'signed_by': sign_prof,
            'evaluation_end': datetime.now()
        })

    @classmethod
    @ModelView.button_action('health_rounding.report_round')
    def print_round(cls, records):
        pass

    @classmethod
    def create(cls, vlist):
        config = Pool().get('health.configuration').get_config()

        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('code'):
                if not config.patient_rounding_sequence:
                    raise UserError(gettext(
                        'health_nursing.msg_missing_rounding_sequence'
                    ))
                values['code'] = config.patient_rounding_sequence.get()
        return super(PatientRounding, cls).create(vlist)

    @classmethod
    def validate(cls, roundings):
        super(PatientRounding, cls).validate(roundings)
        for rounding in roundings:
            rounding.check_health_professional()

    def check_health_professional(self):
        if not self.health_professional:
            raise UserWarning(gettext('health.msg_health_professional_warning'))

    def get_report_start_date(self, name):
        Company = Pool().get('company.company')
        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)

        dt = self.evaluation_start
        return datetime.astimezone(dt.replace(tzinfo=pytz.utc), timezone).date()

    def get_report_start_time(self, name):
        Company = Pool().get('company.company')

        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)

        dt = self.evaluation_start.replace(tzinfo=pytz.utc)
        return datetime.astimezone(dt, timezone).time()

    def get_report_end_date(self, name):
        Company = Pool().get('company.company')

        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)

        dt = self.evaluation_end
        return datetime.astimezone(dt.replace(tzinfo=pytz.utc), timezone).date()

    def get_report_end_time(self, name):
        Company = Pool().get('company.company')
        timezone = None
        company_id = Transaction().context.get('company')
        if company_id:
            company = Company(company_id)
            if company.timezone:
                timezone = pytz.timezone(company.timezone)

        dt = self.evaluation_end.replace(tzinfo=pytz.utc)
        return datetime.astimezone(dt, timezone).time()

    def get_warn_icon(self, name):
        if self.warning:
            return 'health-warning'

    def get_medications(self, name):
        if self.registration:
            return [med.id for med in self.registration.medications]


class RoundingProcedure(ModelSQL, ModelView):
    'Rounding - Procedure'
    __name__ = 'health.rounding_procedure'
    rounding = fields.Many2One('health.patient.rounding', 'Rounding')
    procedure = fields.Many2One('health.procedure', 'Code', required=True,
        select=True,
        help="Procedure Code, for example ICD-10-PCS Code 7-character string")
    notes = fields.Text('Notes')


class PatientAmbulatoryCare(ModelSQL, ModelView):
    'Patient Ambulatory Care'
    __name__ = 'health.patient.ambulatory_care'

    STATES = {'readonly': Eval('state') == 'done'}

    code = fields.Char('ID', readonly=True)
    patient = fields.Many2One('health.patient', 'Patient', required=True,
        states=STATES)
    state = fields.Selection([
        (None, ''),
        ('draft', 'In Progress'),
        ('done', 'Done'),
        ], 'State', readonly=True)
    base_condition = fields.Many2One('health.pathology', 'Condition',
        states=STATES)
    evaluation = fields.Many2One('health.patient.evaluation',
        'Related Evaluation', domain=[('patient', '=', Eval('patient'))],
        depends=['patient'], states=STATES)
    ordering_professional = fields.Many2One('health.professional',
        'Requested by', states=STATES)
    health_professional = fields.Many2One('health.professional',
        'Health Professional', readonly=True)
    procedures = fields.One2Many('health.ambulatory_care_procedure', 'name',
        'Procedures', states=STATES,
        help="List of the procedures in this session. Please enter the first "
        "one as the main procedure")
    session_number = fields.Integer('Session #', states=STATES)
    session_start = fields.DateTime('Start', required=True, states=STATES)

    # Vital Signs
    systolic = fields.Integer('Systolic Pressure', states=STATES)
    diastolic = fields.Integer('Diastolic Pressure', states=STATES)
    bpm = fields.Integer('Heart Rate',states=STATES,
        help='Heart rate expressed in beats per minute')
    respiratory_rate = fields.Integer('Respiratory Rate',states=STATES,
        help='Respiratory rate expressed in breaths per minute')
    osat = fields.Integer('Oxygen Saturation',states=STATES,
        help='Oxygen Saturation(arterial).')
    temperature = fields.Float('Temperature',states=STATES,
        help='Temperature in celsius')

    warning = fields.Boolean('Warning', help="Check this box to alert the "
        "supervisor about this session. A warning icon will be shown in the "
        "session list",states=STATES)
    warning_icon = fields.Function(fields.Char('Warning Icon'), 'get_warn_icon')

    #Glycemia
    glycemia = fields.Integer('Glycemia', help='Blood Glucose level',
        states=STATES)
    evolution = fields.Selection([
        (None, ''),
        ('initial', 'Initial'),
        ('n', 'Status Quo'),
        ('i', 'Improving'),
        ('w', 'Worsening'),
        ], 'Evolution', help="Check your judgement of current "
        "patient condition", sort=False, states=STATES)
    session_end = fields.DateTime('End', readonly=True)
    next_session = fields.DateTime('Next Session', states=STATES)
    session_notes = fields.Text('Notes', states=STATES)
    signed_by = fields.Many2One('health.professional', 'Signed by',
        readonly=True, states={'invisible': Equal(Eval('state'), 'draft')},
        help="Health Professional that signed the session")

    @staticmethod
    def default_health_professional():
        return Pool().get('health.professional').get_health_professional()

    @staticmethod
    def default_session_start():
        return datetime.now()

    @staticmethod
    def default_state():
        return 'draft'

    @classmethod
    def __setup__(cls):
        super(PatientAmbulatoryCare, cls).__setup__()
        cls._buttons.update({
            'end_session': {
                'invisible': ~Eval('state').in_(['draft']),
            }})
        cls._order.insert(0, ('session_start', 'DESC'))

    @classmethod
    @ModelView.button
    def end_session(cls, sessions):
        # End the session and discharge the patient
        Professional = Pool().get('health.professional')
        signing_hp = Professional.get_health_professional()
        # Change the state of the session to "Done"
        cls.write(sessions, {
            'state': 'done',
            'signed_by': signing_hp,
            'session_end': datetime.now()
        })

    @classmethod
    def validate(cls, records):
        super(PatientAmbulatoryCare, cls).validate(records)
        for record in records:
            record.check_health_professional()

    def check_health_professional(self):
        if not self.health_professional:
            raise UserWarning(gettext('health.msg_health_professional_warning'))

    @classmethod
    def create(cls, vlist):
        config = Pool().get('health.configuration').get_config()
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('code'):
                values['code'] = config.ambulatory_care_sequence.get()
        return super(PatientAmbulatoryCare, cls).create(vlist)

    @classmethod
    def copy(cls, ambulatorycares, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['session'] = None
        default['session_start'] = cls.default_session_start()
        default['session_end'] = cls.default_session_start()
        return super(PatientAmbulatoryCare, cls).copy(ambulatorycares,
            default=default)

    def get_warn_icon(self, name):
        if self.warning:
            return 'health-warning'


class AmbulatoryCareProcedure(ModelSQL, ModelView):
    'Ambulatory Care Procedure'
    __name__ = 'health.ambulatory_care_procedure'
    session = fields.Many2One('health.patient.ambulatory_care', 'Session')
    procedure = fields.Many2One('health.procedure', 'Code', required=True,
        select=True,
        help="Procedure Code, for example ICD-10-PCS Code 7-character string")
    comments = fields.Char('Comments')


class KindNursingNote(ModelSQL, ModelView):
    'Kind Nursing Note'
    __name__ = 'health.nursing.note.kind'
    name = fields.Char('Name')


class NursingNote(ModelSQL, ModelView):
    'Nursing Note'
    __name__ = 'health.nursing.note'
    registration = fields.Many2One('health.inpatient', 'Inpatient', required=True)
    note = fields.Text('Note', required=True)
    kind = fields.Many2One('health.nursing.note.kind', 'Kind', required=True)
    attending_physician = fields.Many2One('health.professional',
        'Attending Physician', required=True)
    attending_date = fields.DateTime('Attending Date', readonly=True)

    @classmethod
    def __setup__(cls):
        super(NursingNote, cls).__setup__()
        cls._buttons.update({
                'print_note': {},
                })

    @classmethod
    @ModelView.button_action('health_rounding.report_note')
    def print_note(cls, records):
        pass

    @staticmethod
    def default_attending_date():
        return datetime.now()

    @classmethod
    def default_attending_physician(cls):
        Professional = Pool().get('health.professional')
        cls.attending_physician = Professional.get_health_professional()
        return cls.attending_physician
