
from trytond.pool import Pool
from . import nursing
from . import configuration
from . import inpatient


def register():
    Pool.register(
        configuration.HealthConfiguration,
        nursing.PatientRounding,
        nursing.RoundingProcedure,
        nursing.PatientAmbulatoryCare,
        nursing.AmbulatoryCareProcedure,
        nursing.NursingNote,
        nursing.KindNursingNote,
        inpatient.Inpatient,
        inpatient.PatientNutritionControl,
        inpatient.PatientWaterBalance,
        inpatient.PatientWaterBalanceShift,
        module='health_nursing', type_='model')
