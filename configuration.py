# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta


class HealthConfiguration(metaclass=PoolMeta):
    "Health Configuration"
    __name__ = "health.configuration"
    ambulatory_care_sequence = fields.Many2One('ir.sequence',
        'Health Ambulatory Care')
    patient_rounding_sequence = fields.Many2One('ir.sequence',
        'Health Rounding')
